﻿.mode column
.headers on
.separator ROW "\n"
.nullvalue NULL

PRAGMA foreign_keys = ON;

CREATE TABLE University (
	"universityID" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name" VARCHAR(100) NOT NULL,
	"Address" VARCHAR(100)
);

CREATE TABLE Restaurant (
	"restaurantID" INTEGER PRIMARY KEY AUTOINCREMENT,
	"universityID" INTEGER NOT NULL,
	"Name" VARCHAR(100) NOT NULL,
	"Address" VARCHAR(100) NOT NULL,
	"Available" INTEGER DEFAULT 1 CHECK("Available" > -1 AND "Available" < 2),

	FOREIGN KEY("universityID") REFERENCES "University"("universityID")
);

CREATE TABLE Maintenance (
	"userID" INTEGER NOT NULL,
	"restaurantID" INTEGER NOT NULL,

	PRIMARY KEY("userID", "restaurantID"),
	FOREIGN KEY ("userID") REFERENCES "User"("userID") ON DELETE CASCADE,
	FOREIGN KEY("restaurantID") REFERENCES "Restaurant"("restaurantID") ON DELETE  CASCADE
);

CREATE TABLE Meal (
	"mealID" INTEGER PRIMARY KEY AUTOINCREMENT,
	"restaurantID" INTEGER NOT NULL,
	"Name" VARCHAR(100) NOT NULL,
	"Type" INTEGER NOT NULL CHECK("Type" > 0 AND "Type" < 4),
	"LactoseFree" BOOLEAN DEFAULT 0,
	"GlutenFree" BOOLEAN DEFAULT 0,

	FOREIGN KEY("restaurantID") REFERENCES "Restaurant"("restaurantID") ON DELETE CASCADE
);

CREATE TABLE Menu (
	"menuID" INTEGER PRIMARY KEY AUTOINCREMENT,
	"restaurantID" INTEGER NOT NULL,

	FOREIGN KEY("restaurantID") REFERENCES "Restaurant"("restaurantID") ON DELETE CASCADE
);

CREATE TABLE HasDate (
	"Date" DATE NOT NULL,
	"menuID" INTEGER NOT NULL,

	PRIMARY KEY("Date", "menuID"),
	FOREIGN KEY("menuID") REFERENCES "Menu"("menuID") ON DELETE CASCADE
);

CREATE TABLE OnList (
	"menuID" INTEGER NOT NULL,
	"mealID" INTEGER NOT NULL,

	PRIMARY KEY("menuID", "mealID"),
	FOREIGN KEY("menuID") REFERENCES "Menu"("menuID") ON DELETE CASCADE,
	FOREIGN KEY("mealID") REFERENCES "Meal"("mealID") ON DELETE CASCADE
);

CREATE TABLE User (
	"userID" INTEGER PRIMARY KEY AUTOINCREMENT,
	"Name" VARCHAR(100) NOT NULL,
	"Email" VARCHAR(100) UNIQUE NOT NULL,
	"Password" VARCHAR(20) NOT NULL CHECK(length("Password") >= 4 AND length("Password") <= 20),
	"IsAdmin" INTEGER NOT NULL CHECK("IsAdmin" > -1 AND "IsAdmin" < 2)
);

CREATE TABLE Review (
	"reviewID" INTEGER PRIMARY KEY AUTOINCREMENT,
	"userID" INTEGER NOT NULL,
	"mealID" INTEGER NOT NULL,
	"Grade" INTEGER NOT NULL CHECK ("Grade" > 0 AND "Grade" < 6),
	"VerbalEvaluation" VARCHAR(200),
	"PositiveVote" INTEGER DEFAULT 0,
	"NegativeVote" INTEGER DEFAULT 0,
	"Date" DATE NOT NULL,
	"Time" TIME NOT NULL,
	"Showable" INTEGER DEFAULT 1 CHECK ("Showable" > -1 AND "Showable" < 2),

	FOREIGN KEY("userID") REFERENCES "User"("userID"),
	FOREIGN KEY("mealID") REFERENCES "Meal"("mealID") ON DELETE CASCADE
);

INSERT INTO "University" ("Name", "Address")
	VALUES ("LUT-Yliopisto", "Yliopistonkatu 34");

INSERT INTO "Restaurant" ("universityID", "Name", "Address")
	VALUES ("1", "Laseri", "Laserkatu 6");

INSERT INTO "Restaurant" ("universityID", "Name", "Address")
	VALUES ("1", "Yolo", "Laserkatu 10");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("1", "Broileriwokki", "1", "0", "1");

INSERT INTO "Meal" ("restaurantID", "Name", "Type")
	VALUES ("1", "Possuposki-paprikapata", "2");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("1", "Lasagne", "1", "0", "1");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("1", "Kalkkunaa hunaja-kookokastikkeessa", "2", "1", "0");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("1", "Kasvislasagne", "3", "1", "0");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("1", "Tofu-pähkinä-kasviswokki", "3", "0", "1");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("2", "Koskenlaskijan kalaa", "2", "1", "1");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("2", "Jauheliharisotto", "1", "1", "1");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("2", "Thai kasviscurrya", "3", "1", "1");

INSERT INTO "Meal" ("restaurantID", "Name", "Type", "LactoseFree", "GlutenFree")
	VALUES ("2", "Kinkkukiusaus", "1", "1", "1");

INSERT INTO "Menu" ("restaurantID")
	VALUES ("1");

INSERT INTO "Menu" ("restaurantID")
	VALUES ("2");

INSERT INTO "HasDate"
	VALUES ("2019-07-01", "1");

INSERT INTO "HasDate"
	VALUES ("2019-07-01", "2");

INSERT INTO "OnList"
	VALUES ("1", "1");

INSERT INTO "OnList"
	VALUES ("1", "2");

INSERT INTO "OnList"
	VALUES ("1", "5");

INSERT INTO "OnList"
	VALUES ("2", "8");

INSERT INTO "OnList"
	VALUES ("2", "7");

INSERT INTO "OnList"
	VALUES ("2", "9");

INSERT INTO "User" ("Name", "Email", "Password", "IsAdmin")
	VALUES ("TestAdmin", "testadmin@email.com", "adminPassword", "1");

INSERT INTO "Maintenance"
	VALUES ("1", "1");

INSERT INTO "Maintenance"
	VALUES ("1", "2");

INSERT INTO "User" ("Name", "Email", "Password", "IsAdmin")
	VALUES ("TestUser", "testuser@gmail.com", "userPassword", "0");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "2", "Mautonta.", "10", "2019-07-01", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "3", "Ihan ok.", "1", "2019-07-02", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "4", "Oli hyvää.", "2", "2019-07-03", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "5", "Lempiruokaani!", "5", "2019-07-27", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "1", "Hirveää.", "9", "2019-08-01", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "3", "Perusruokaa", "7", "2019-08-01", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "4", "Reseptiä on varmaan paranneltu!", "7", "2019-08-02", "13:23:07");

INSERT INTO "Review" ("userID", "Grade", "VerbalEvaluation", "mealID", "Date", "Time")
	VALUES ("2", "4", "Edelleen hyvää", "7", "2019-08-03", "13:23:07");



-- Menun hakeminen, where-ehdolla voi vaikuttaa paivaan ja ravintolaan
SELECT Restaurant.Name AS "Restaurant", HasDate.Date, Meal.Name AS "Meal", Meal.LactoseFree AS "Lactose free", Meal.GlutenFree AS "Gluten Free"
	FROM (((((HasDate)
	INNER JOIN Menu ON HasDate.menuID = Menu.menuID)
	INNER JOIN OnList ON Menu.menuID = OnList.menuID)
	INNER JOIN Meal ON OnList.mealID = Meal.mealID)
	INNER JOIN Restaurant ON Meal.restaurantID = Restaurant.restaurantID)
	WHERE HasDate.Date = "2019-07-01"
ORDER BY "HasDate.Date", "Restaurant", "Meal.Type";

-- Arvostelujen hakeminen, where-ehdolla voi vaikuttaa mita haetaan ja Order by –lauseella miten jarjestellaan (oletuskena aakkosellisesti)
SELECT Meal.Name AS "Meal", Restaurant.Name AS "Restaurant", Review.Grade AS "Grade", Review.VerbalEvaluation AS "Verbal evaluation", Review.Date AS "Date"
	FROM (Review OUTER LEFT JOIN Meal ON Review.mealID = Meal.mealID)
	OUTER LEFT JOIN Restaurant ON Meal.restaurantID = Restaurant.restaurantID
	WHERE Review.Date > "2019-06-30"
	ORDER BY "Restaurant", "Meal";

-- Tasta vois tehda View:n, mutta haetaan kaikkien ruokien keskiarvosana
SELECT Meal.Name AS "Meal", Restaurant.Name AS "Restaurant", ROUND(AVG(Review.Grade), 1) AS "Average grade"
	FROM (Review OUTER LEFT JOIN Meal ON Review.mealID = Meal.mealID)
	OUTER LEFT JOIN Restaurant ON Meal.restaurantID = Restaurant.restaurantID
	WHERE Review.Date > "2019-06-30"
	GROUP BY Meal.Name
	ORDER BY "Average grade" DESC;

-- Yllapitajan nakokulma, nakee ravintoloiden tiedot
SELECT Restaurant.Name AS "Restaurant", Restaurant.Address AS "Address", University.Name AS "University"
	FROM Restaurant
	INNER JOIN University ON Restaurant.universityID = Restaurant.universityID
	ORDER BY "Restaurant";

-- Yllapitajan nakokulma, voi hakea kayttajien tiedot
SELECT User.Name AS "Name", User.Email AS "Email"
	FROM User
	ORDER BY Name;	

UPDATE "Review"
	SET "Showable" = 0
	WHERE "Grade" < "3";

-- Yllapitajan nakokulma, tietyn kayttajan arvosteluiden hakeminen (Where-lauseella voidaan tarkastella showablen perusteella)
SELECT User.Name AS "Name", Meal.Name AS "Meal", Restaurant.Name AS "Restaurant", Review.Grade AS "Grade", Review.VerbalEvaluation AS "Verbal evaluation", Review.Date AS "Date", Review.Showable AS "Showable"
	FROM ((Review OUTER LEFT JOIN Meal ON Review.mealID = Meal.mealID)
	OUTER LEFT JOIN Restaurant ON Meal.restaurantID = Restaurant.restaurantID)
	OUTER LEFT JOIN User ON Review.userID = User.userID
	WHERE User.Name = "TestUser"
	ORDER BY "Name", "Restaurant", "Meal";

